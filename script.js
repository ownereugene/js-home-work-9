function createList(array) {
  const elementUl = document.createElement("ul");
  document.body.appendChild(elementUl);

  let elementsLi=[];
  let i = 0;
  for (const item of array) {
      let strUl = "";
      if (typeof (item) !== "object") {
          elementsLi[i++]= `<li>${item}</li>`;
      } else {
          if (Array.isArray(item)) {
              for (let nestedItem of item) {
                  strUl += `<li>${nestedItem}</li>`;
              }
          } else {
              for (let nestedItem in item) {
                  strUl += `<li>${item[nestedItem]}</li>`;
              }
          }

          elementsLi[i++]= `<ul>${strUl}</ul>`
      }
  }
  for (let li of elementsLi) {
      elementUl.innerHTML += li;
  }


}
createList(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv", ["NY", "LA", "DC", "CG"], {name: "123"}]);

let button = document.createElement("button");
button.innerText = "Clear";
divElem = document.createElement("div");
document.body.appendChild(divElem);
document.body.appendChild(button);
button.setAttribute("onclick", "clearPage()");
function clearPage() {
  let sec = 4;
  setInterval(tick,1000);
  function tick(){
      divElem.innerText = "left "+(--sec)+" seconds";
  }
  setTimeout(function() {
      document.querySelector("body").outerHTML = "";
  }, 4000);
}